# Shopping list
This project provides the front-end for a React web-app and does not (yet) include back-end code. The purpose of the project was to practice using Redux toolkit for state management (instead of only useState()). 


## Dependencies
- react
- react-redux'
- @reduxjs/toolkit
- react-icons
- uuid