import './App.css';
import NewItem from './components/NewItem'
import ShoppingList from './components/ShoppingList'

function App() {
  return (
    <div className="App">
    <h1>Shopping List</h1>
      <NewItem />
      <ShoppingList/>
    </div>
  );
}

export default App;