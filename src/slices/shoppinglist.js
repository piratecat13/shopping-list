import { createSlice } from '@reduxjs/toolkit'
import {current } from "@reduxjs/toolkit"

export const listSlice = createSlice({
    name: 'shoppinglist',
    initialState: {
        shoppinglist: []
    },
    reducers: {
        addItem: (state, action) => {
            state.shoppinglist.push(action.payload)
            console.log(current(state))
        },

        deleteItem: (state, action) => {
            const temp =  state.shoppinglist.filter(item => item.id !== action.payload)
            state.shoppinglist = temp
        },

        toggle: (state, action) => {
            state.shoppinglist.map(item => { 
              if (item.id === action.payload) {
                  item.complete = !item.complete;
                }
                return state
            })
        },
    }
})

export const { addItem, deleteItem, toggle } = listSlice.actions
  
export default listSlice.reducer
