import { configureStore } from '@reduxjs/toolkit'
import listReducer from './shoppinglist'

export default configureStore({
    reducer: {
        shoppinglist: listReducer,
    } 
})