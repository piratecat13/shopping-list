import React from 'react'
import { useDispatch } from 'react-redux';
import { addItem } from '../slices/shoppinglist'
import { useState } from 'react';
import {FiPlusSquare} from "react-icons/fi";
const {v4 : uuidv4} = require('uuid')


export default function NewItem() {

  const dispatch = useDispatch();
  const [input, setInput] = useState({ title: '', id: '', complete: false });

  const handleSubmit = e => {
    e.preventDefault();
    if (input.title !== '') {
      dispatch(addItem(input))
    }
    setInput({ title: '', id: '', complete: false })
  }

  const handleChange = e => {
    setInput({...input, [e.target.name]: e.target.value, id: uuidv4()})
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className='input'>
          <input 
            className='input-field'
            type="text"
            name="title"
            value={input.title}
            onChange={handleChange}
            placeholder="add new item"
            size="50"
            maxLength="100"
            />
          <FiPlusSquare onClick={handleSubmit}/>
        </div>
      </form>
    </>
  )
}
