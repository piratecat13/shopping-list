import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { deleteItem, toggle } from '../slices/shoppinglist'
import {FiXCircle} from "react-icons/fi";
import {FiCheckSquare} from "react-icons/fi";
import {FiSquare} from "react-icons/fi";

export default function ShoppingList() {

  const { shoppinglist } = useSelector(state => state.shoppinglist)
  const dispatch = useDispatch();

  return (
    <>
      <br/>
        <div className='list-container'>
          <div>{ shoppinglist?.map(item => (
              <div
                key={item.id}
                className='item-container'>
                
                <div className='toggle'>
                    {item.complete? (
                      <div className='complete'>
                        <FiCheckSquare 
                            key={item.id}
                            onClick={() => dispatch(toggle(item.id))}
                        />
                      </div>
                      ) : (
                      <div className='not-complete'>
                        <FiSquare
                          key={item.id}
                          onClick={() => dispatch(toggle(item.id))}
                        />
                      </div>
                    )}
                </div>

                <div className='title'>
                  <div className={item.complete? 'complete': 'not-complete'}>
                    <label>{item.title}</label>
                  </div>
                </div>

                <div className='delete'>
                    <FiXCircle
                        key={item.id} 
                        onClick={() => dispatch(deleteItem(item.id))} />
                </div>
              </div>
            ))}
        </div>
      </div>
    </>
  )
}
